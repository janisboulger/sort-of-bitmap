Sort the Bitmap
=============

[HD CCTV System](https://www.jmcsecure.co.uk/)

What does it mean then?
------------

First locate your pictures then place it into the browser so that they will processed easily. Now what will happen to every picture of yours is very simple. 

    Compare each and every pixel below it so that your picture looks good
        The threshold value in the brightness will be changed
            Change the pixels using alpha blending easily
    Repeat the process

Advantages:
-----

* good quality
* QueryHGD
* Date.HGD

![file.png](https://bitbucket.org/repo/66q4br/images/977858653-file.png)